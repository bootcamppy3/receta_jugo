const express = require('express');
const router = express.Router();
const JuiceRecipe = require('../models/JuiceRecipe');

// Aquí irán las rutas

router.get('/juice-recipes', async (req, res) => {
  try {
    const juiceRecipes = await JuiceRecipe.find();
    res.json(juiceRecipes);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

router.post('/juice-recipes', async (req, res) => {
  const juiceRecipe = new JuiceRecipe({
    name: req.body.name,
    ingredients: req.body.ingredients,
    instructions: req.body.instructions
  });
  try {
    const newJuiceRecipe = await juiceRecipe.save();
    res.status(201).json(newJuiceRecipe);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

router.put('/juice-recipes/:id', async (req, res) => {
  try {
    const juiceRecipe = await JuiceRecipe.findByIdAndUpdate(
      req.params.id,
      {
        name: req.body.name,
        ingredients: req.body.ingredients,
        instructions: req.body.instructions
      },
      { new: true }
    );
    res.json(juiceRecipe);
  } catch (err) {
    res.status(404).json({ message: err.message });
  }
});

router.delete('/juice-recipes/:id', async (req, res) => {
  try {
    await JuiceRecipe.findByIdAndDelete(req.params.id);
    res.json({ message: 'Receta eliminada' });
  } catch (err) {
    res.status(404).json({ message: err.message });
  }
});

module.exports = router;