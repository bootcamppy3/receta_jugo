const express = require('express');
const app = express();

app.use(express.json()); // Este middleware es necesario para procesar solicitudes con formato JSON

app.use(express.urlencoded({ extended: true })); // Este middleware es necesario para procesar solicitudes con datos de formulario

const PORT = process.env.PORT || 3000; // Utiliza el puerto definido en la variable de entorno PORT, o utiliza el puerto 3000 por defecto
app.listen(PORT, () => {
  console.log(`Servidor escuchando en el puerto ${PORT}`);
});